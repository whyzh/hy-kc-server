-- ----------------------------
-- Table structure for hazard
-- ----------------------------
DROP TABLE IF EXISTS `hazard`;
CREATE TABLE `hazard`  (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `name` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
    `code` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '设备编码',
     type tinyint(2) NOT NULL COMMENT '类型：1- 设备  2-存储设施  3-场所',
    `grade` tinyint(2) NOT NULL DEFAULT 1 COMMENT '设备等级：1- 一般设备，2- 主要设备，3- 核心设备',
    `building` varchar(100) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '使用单位',
    `describe` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '介绍描述',
    `remark` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '备注',
    `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态，0：禁用，1：启用',
    `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人ID',
    `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人ID',
    `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除，0：未删除，1：已删除',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COMMENT = '设备列表' ROW_FORMAT = DYNAMIC;


-- ----------------------------
-- Table structure for hazard_unit
-- ----------------------------
DROP TABLE IF EXISTS `hazard_unit`;

CREATE TABLE `hazard_unit`  (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `hazard_id` bigint(20) NOT NULL COMMENT '设备id',
    `name` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '部件名称',
    `unit_code` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '部件编码',
    `manufacturers` varchar(100) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '生产单位',
    `model` varchar(100) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '型号',
    `specifications` varchar(100) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '规格',
    `durable_years` varchar(100) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '使用年限',
    `use_time` datetime NULL DEFAULT NULL COMMENT '投入使用时间',
    `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否正常使用，0：未使用，1：使用中 2-维修中 3-报废',
    `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人ID',
    `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人ID',
    `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除，0：未删除，1：已删除',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `hazard_id`(`hazard_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COMMENT = '设备安全隐患部件列表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
     `id` bigint(20) NOT NULL COMMENT '主键',
     `name` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
     `describe` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '介绍描述',
     `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人ID',
     `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人ID',
     `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除，0：未删除，1：已删除',
     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COMMENT = '标签列表' ROW_FORMAT = DYNAMIC;


-- ----------------------------
-- Table structure for secure_event
-- ----------------------------
DROP TABLE IF EXISTS `secure_event`;
CREATE TABLE `secure_event`  (
     `id` bigint(20) NOT NULL COMMENT '主键',
     `type` tinyint(2) NOT NULL COMMENT '风险事件等级：1- 严重，2- 重要，3- 一般，4- 轻微',
     `name` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '隐患事项名',
     `tags` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '标签, 多个标签用逗号分隔',
     `describe` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '介绍描述',
     `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人ID',
     `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人ID',
     `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除，0：未删除，1：已删除',
     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COMMENT = '设备部件存在安全隐患事件列表' ROW_FORMAT = DYNAMIC;



-- ----------------------------
-- Table structure for cntrol_measure
-- ----------------------------
DROP TABLE IF EXISTS `cntrol_measure`;
CREATE TABLE `cntrol_measure`  (
       `id` bigint(20) NOT NULL COMMENT '主键',
       `name` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '管控方案名',
       `tags` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '标签, 多个标签用逗号分隔',
       `cycle` int(10) NULL DEFAULT 0 COMMENT '检查周期(天)',
       `describe` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '介绍描述',
       `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人ID',
       `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
       `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人ID',
       `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
       `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除，0：未删除，1：已删除',
       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COMMENT = '安全管控措施方案' ROW_FORMAT = DYNAMIC;



-- ----------------------------
-- Table structure for hazard_unit_event
-- ----------------------------
DROP TABLE IF EXISTS `hazard_unit_event`;
CREATE TABLE `hazard_unit_event`  (
      `id` bigint(20) NOT NULL COMMENT '主键',
      `hazard_id` bigint(20) NOT NULL COMMENT '设备id',
      `unit_id` bigint(20) NOT NULL COMMENT '部件id',
      `event_id` bigint(20) NOT NULL COMMENT '安全隐患id',
      `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人ID',
      `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
      `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人ID',
      `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
      `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除，0：未删除，1：已删除',
      PRIMARY KEY (`id`) USING BTREE,
      INDEX `hazard_unit_id`(`hazard_id`, `unit_id`) USING BTREE,
      INDEX `event_id`(`event_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COMMENT = '设备部件存在安全隐患事件关联关系' ROW_FORMAT = DYNAMIC;


-- ----------------------------
-- Table structure for hazard_unit_event_record
-- ----------------------------
DROP TABLE IF EXISTS `hazard_unit_event_record`;
CREATE TABLE `hazard_unit_event_record`  (
     `id` bigint(20) NOT NULL COMMENT '主键',
     `event_id` bigint(20) NOT NULL COMMENT '安全隐患id',
     `detection_user_id` bigint(20) NULL DEFAULT NULL COMMENT '检测人员ID',
     `detection_time` datetime NULL DEFAULT NULL COMMENT '检测时间',
     `way_type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '检测方式，1：现场检测，2：拍照，3：录像',
     `remark` varchar(200) CHARACTER SET utf8mb4 NULL DEFAULT NULL COMMENT '检测反馈备注',
     `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否已经处理，0：待处理，1：已处理',
     `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人ID',
     `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `update_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人ID',
     `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除，0：未删除，1：已删除',
     PRIMARY KEY (`id`) USING BTREE,
     INDEX `event_id`(`event_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COMMENT = '安全隐患事件处理记录' ROW_FORMAT = DYNAMIC;

