package io.geekidea.boot.common.constant;

/**
 * @author mr.wei
 * @date 2022/7/9
 **/
public interface RedisKey {

    /**
     * 登录token
     */
    String LOGIN_TOKEN = "login:token:%s";

}
