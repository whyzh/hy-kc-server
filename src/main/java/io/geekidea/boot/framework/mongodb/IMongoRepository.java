package io.geekidea.boot.framework.mongodb;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;

@NoRepositoryBean
public interface IMongoRepository<T, ID> extends MongoRepository<T, ID> {
 
    String getTableName();
    void deleteAll(Collection<ID> ids);
    MongoOperations getMongoOperations();
    MongoEntityInformation<T, ID> getEntityInformation();
    Class<T> getEntityClass();
    Class<ID> getIdClass();
}