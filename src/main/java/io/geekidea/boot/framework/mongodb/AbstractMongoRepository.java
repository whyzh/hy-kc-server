package io.geekidea.boot.framework.mongodb;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;

import java.util.Collection;

public class AbstractMongoRepository<T, ID> extends SimpleMongoRepository<T, ID> implements IMongoRepository<T, ID> {
 
    private final MongoOperations mongoOperations;
    private final MongoEntityInformation<T, ID> entityInformation;
 
    public AbstractMongoRepository(MongoEntityInformation<T, ID> metadata, MongoOperations mongoOperations) {
        super(metadata, mongoOperations);
        this.mongoOperations = mongoOperations;
        this.entityInformation = metadata;
    }

    @Override
    public String getTableName(){
        return entityInformation.getCollectionName();
    }
 
 
    @Override
    public void deleteAll(Collection<ID> ids) {
        ids.forEach(this::deleteById);
    }
 
 
    @Override
    public MongoOperations getMongoOperations() {
        return mongoOperations;
    }
 
    @Override
    public MongoEntityInformation<T, ID> getEntityInformation() {
        return entityInformation;
    }
 
    @Override
    public Class<T> getEntityClass() {
        return entityInformation.getJavaType();
    }
 
    @Override
    public Class<ID> getIdClass() {
        return entityInformation.getIdType();
    }
}