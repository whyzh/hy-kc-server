package io.geekidea.boot.framework.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础对象抽象类
 *
 * @author shusong.liang
 */
@Data
public abstract class BaseVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private String id;

    @Schema(description = "创建时间")
    private Date createDate;

    @Schema(description = "修改时间")
    private Date updateTime;
}
