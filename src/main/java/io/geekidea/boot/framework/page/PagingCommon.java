package io.geekidea.boot.framework.page;

import io.geekidea.boot.framework.enums.PageTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
@Schema(description = "分页请求基类")
public abstract class PagingCommon implements Serializable {

    private static final long serialVersionUID = -5940911579375929368L;

    /**
     * 分页时间
     */
    @Schema(description = "分页时间时间戳   1591623638000   当前时间 创建时间降序排序 ")
    private Long createDateTime;

    /**
     * 每页显示条数
     */
    @Schema(description = "每页显示条数")
    private Integer pageSize;

    /**
     * 1:下一页 2: 上一页
     */
    @Schema(description = "1: 下一页 2: 上一页")
    private Integer pageType;


    public Integer getPageSize() {
        //此处限制最大查询100条 防止恶意查询
        return pageSize == null || pageSize < 1 || pageSize > 100 ? 10 : pageSize;
    }
    public PageTypeEnum getPageTypeEnum() {
        PageTypeEnum anEnum = PageTypeEnum.getEnum(pageType);
        return anEnum == null ? PageTypeEnum.DOWN : anEnum;
    }

    /**
     * 是否下一页
     *
     * @return
     */
    public Boolean isDown() {
        return getCreateDateTime()!=null && PageTypeEnum.DOWN.equals(getPageTypeEnum());
    }

    /**
     * 是否上一页
     *
     * @return
     */
    public Boolean isUp() {
        return getCreateDateTime()!=null && PageTypeEnum.UP.equals(getPageTypeEnum());
    }
}
