package io.geekidea.boot.framework.page;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author shusong.liang
 * @Description
 * @Date 2022/04/11 10:59
 **/
@Data
public class PageRequest implements Serializable {

    /**
     * 默认页号
     */
    public static final int DEFAULT_PAGE_NUM = 1;

    /**
     * 默认分页大小
     */
    public static final int DEFAULT_PAGE_SIZE = 10;

    /**
     * 指定页数, 这个字段有值时忽略 first和 last参数, 使用 mongo的skip().limit()方式分页(扫描全文档,影响性能)
     */
    @Schema(description = "第几页", example = "1")
    private Integer pageNum = DEFAULT_PAGE_NUM;

    /**
     * 每页记录数, 默认10
     */
    @Schema(description = "每页记录数, 默认10", example = "10")
    private int pageSize = DEFAULT_PAGE_SIZE;

    /**
     * 方向 向上：1  向下：2
     */
    @Schema(description = "方向 向上：1  向下：2(APP端使用)", example = "1", hidden = true)
    private int direction = PageDirection.NEXT;

    /**
     * 第一个元素标记
     */
    @Schema(description = "第一个元素标记(APP端使用)", example = "1", hidden = true)
    private String firstId;

    /**
     * 最后一个元素标记
     */
    @Schema(description = "最后一个元素标记(APP端使用)", example = "2", hidden = true)
    private String lastId;

    public void setPageSize(int pageSize) {
        if (pageSize <= 0) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        this.pageSize = pageSize;
    }


    public interface PageDirection {

        /**
         * 向前翻页
         */
        int PREVIOUS = 1;

        /**
         * 向后翻页
         */
        int NEXT = 2;
    }

    public static PageRequest buildDefault(Integer pageNum, Integer pageSize) {
        PageRequest pageRequest = new PageRequest();

        if (pageNum == null || pageNum < 1) {
            pageNum = 1;
        }
        pageRequest.setPageNum(pageNum);

        if (pageSize != null) {
            pageRequest.setPageSize(pageSize);
        }

        return pageRequest;
    }

}
