package io.geekidea.boot.framework.page;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author shusong.liang
 * @Description
 **/
@Data
public class PageParams implements Serializable {

    @Schema(description = "每页显示数据条数", example = "10")
    private Integer pageSize = 10;

    /** 方向 向上：1  向下：2 */
    @Schema(description = " 方向 上一页：1  下一页：2 默认为2", example = "2")
    private Integer direction = 2;

    /** 第一个元素标记 */
    @Schema(description = "第一个元素标记")
    private String first;

    /** 最后一个元素标记 */
    @Schema(description = "最后一个元素标记")
    private String last;
}
