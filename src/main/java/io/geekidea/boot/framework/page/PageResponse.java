package io.geekidea.boot.framework.page;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页查询结果
 *
 * @author shusong.liang
 **/
@Data
public class PageResponse<T> implements Serializable {

    /**
     * 当前页
     */
    @Schema(description = "当前页")
    private Integer pageNum = 0;

    /**
     * 每页记录数
     */
    @Schema(description = "每页记录数",example="10")
    private Integer pageSize = 0;

    /**
     * 总条数
     */
    @Schema(description = "总条数")
    private Long totalCount = 0L;

    /**
     * 总页数
     */
    @Schema(description = "总页数")
    private Integer totalPage = 0;

    /**
     * 第一个元素标记 id
     */
    @Schema(description = "第一个元素标记ID")
    private String firstId = "";

    /**
     * 最后一个元素标记 id
     */
    @Schema(description = "最后一个元素标记ID")
    private String lastId = "";

    /**
     * 数据集
     */
    private List<T> data;
}
