package io.geekidea.boot.framework.enums;

import java.util.Arrays;

/**
 * 分页类型
 * @author way
 * @date 2020-12-10
 */
public enum PageTypeEnum {

    /**
     *下一页
     */
    DOWN(1)

    /**
     * 上一页
     */
    ,UP(2);

    private Integer type;

    PageTypeEnum(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public static PageTypeEnum getEnum(Integer value) {
        return Arrays.asList(PageTypeEnum.values()).stream().filter(l -> l.getType().equals(value)).findFirst().orElse(null);
    }
}
