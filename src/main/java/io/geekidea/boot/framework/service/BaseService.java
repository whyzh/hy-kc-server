package io.geekidea.boot.framework.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author mr.wei
 * @date 2022/3/16
 **/
public interface BaseService<T> extends IService<T> {
}
