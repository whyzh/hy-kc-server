package io.geekidea.boot.framework.exception;

/**
 * 登录异常
 *
 * @author mr.wei
 * @date 2018-11-08
 */
public class LoginException extends RuntimeException {

    public LoginException(String message) {
        super(message);
    }

}
