package io.geekidea.boot.framework.exception;

/**
 * 文件上传异常
 *
 * @author mr.wei
 * @date 2023-11-26
 */
public class UploadException extends BusinessException {

    public UploadException(String message) {
        super(message);
    }

}
