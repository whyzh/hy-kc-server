package io.geekidea.boot.framework.exception;

/**
 * 响应状态码
 */
public enum ResponseCode {

    NO_SEARCH_DATA("999", "未查到数据！"),
    NO_HAVE("999", "没有权限！"),
    DATA_CHECKED("998","数据已审核！请勿重复操作"),
    REGISTER_FAILED("999","请勿频繁操作"),
    ;

    /**
     * 请求成功
     */
    public static final String SUCCESS = "000";

    /**
     * 服务器不可预知异常
     */
    public static final String FAILED = "999";



    ResponseCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
