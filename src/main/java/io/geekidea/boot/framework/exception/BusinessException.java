package io.geekidea.boot.framework.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * 业务异常
 *
 * @author mr.wei
 * @date 2018-11-08
 */
@Slf4j
public class BusinessException extends RuntimeException {

    private String code;

    private Object data;
    public BusinessException(String message) {
        super(message);
    }



    /**
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     * @param code    seee the {@link ResponseCode}
     */
    public BusinessException(String message, Object code) {
        super(message);
        this.code = String.valueOf(code);
        StackTraceElement[] classArray = new Exception().getStackTrace();
        if (classArray != null && classArray.length > 1) {
            String classname = classArray[1].getClassName();
            String methodName = classArray[1].getMethodName();
            Integer line = classArray[1].getLineNumber();
            log.error("className:{} | methodName:{} | line:{} | code:{} | message:{} ", classname, methodName, line, code, message);
        }
    }

}
