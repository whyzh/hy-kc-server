package io.geekidea.boot.framework.exception;

/**
 * 权限异常
 *
 * @author mr.wei
 * @date 2018-11-08
 */
public class AuthException extends BusinessException {

    public AuthException(String message) {
        super(message);
    }

}
