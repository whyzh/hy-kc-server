package io.geekidea.boot;

import io.geekidea.boot.util.IpUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.List;

/**
 * 启动类
 *
 * @author mr.wei
 * @date 2022-3-16
 */
@EnableAsync
@SpringBootApplication
public class SpringBootPlusApplication {

    private static final String BACKSLASH = "/";

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = SpringApplication.run(SpringBootPlusApplication.class, args);
            // 打印项目信息
        printlnProjectInfo(context);
    }

    /**
     * 打印项目信息
     *
     * @param context
     */
    private static void printlnProjectInfo(ConfigurableApplicationContext context) {
        try {
            ConfigurableEnvironment environment = context.getEnvironment();
            String serverPort = environment.getProperty("server.port");
            String contextPath = environment.getProperty("server.servlet.context-path");
            if (!BACKSLASH.equals(contextPath)) {
                contextPath = contextPath + BACKSLASH;
            }
            String localhostDocUrl = "\nhttp://localhost:" + serverPort + contextPath + "doc.html";
            System.out.println(localhostDocUrl);
            String localhostSwaggerUrl = "http://localhost:" + serverPort + contextPath + "swagger-ui/index.html";
            System.out.println(localhostSwaggerUrl);
            List<String> ipV4List = IpUtil.getLocalhostIpList();
            if (CollectionUtils.isNotEmpty(ipV4List)) {
                for (String ip : ipV4List) {
                    String ipUrl = "http://" + ip + ":" + serverPort + contextPath + "doc.html";
                    System.out.println(ipUrl);
                }
            }
            System.out.println("\n账号：admin");
            System.out.println("密码：123456");
            System.out.println("swagger密码：e10adc3949ba59abbe56e057f20f883e\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
