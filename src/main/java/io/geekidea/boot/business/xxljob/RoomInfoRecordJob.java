package io.geekidea.boot.business.xxljob;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RoomInfoRecordJob {


    /**
     * 查询天维度每条流的播放数据，包括总流量等。
     * 凌晨3点之后执行
     */
    @XxlJob("dealRoomStatsJob")
    public ReturnT<String> dealStreamDayPlayInfoJob(String param) {
//        String nowDateStr = DateUtil.nowDateTime(DateUtil.Y_M_D_HMS);
//        log.info("xxlJob-start>>>dealStreamDayPlayInfoJob:查询昨天每条流的播放总流量...param={}, 执行时间：{}",param,nowDateStr);
//        roomFluxStatsServiceImpl.saveRoomFluxStats("","");
        log.info("xxlJob-end>>>dealStreamDayPlayInfoJob:查询昨天每条流的播放总流量...结束...");
        return ReturnT.SUCCESS;
    }


}
