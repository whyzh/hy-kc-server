package io.geekidea.boot.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.boot.business.entity.SecureEvent;
import io.geekidea.boot.business.query.SecureEventQuery;
import io.geekidea.boot.business.vo.SecureEventVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 设备部件存在安全隐患事件列 Mapper 接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Mapper
public interface SecureEventMapper extends BaseMapper<SecureEvent> {

    /**
     * 设备部件存在安全隐患事件列详情
     *
     * @param id
     * @return
     */
    SecureEventVo getSecureEventById(Long id);

    /**
     * 设备部件存在安全隐患事件列分页列表
     *
     * @param query
     * @return
     */
    List<SecureEventVo> getSecureEventPage(SecureEventQuery query);

}
