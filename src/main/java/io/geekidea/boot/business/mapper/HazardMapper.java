package io.geekidea.boot.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.boot.business.entity.Hazard;
import io.geekidea.boot.business.query.HazardQuery;
import io.geekidea.boot.business.vo.HazardVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 设备列 Mapper 接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Mapper
public interface HazardMapper extends BaseMapper<Hazard> {

    /**
     * 设备列详情
     *
     * @param id
     * @return
     */
    HazardVo getHazardById(Long id);

    /**
     * 设备列分页列表
     *
     * @param query
     * @return
     */
    List<HazardVo> getHazardPage(HazardQuery query);

    List<HazardVo> getHazardList(HazardQuery query);


}
