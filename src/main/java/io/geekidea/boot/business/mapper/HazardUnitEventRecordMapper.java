package io.geekidea.boot.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.boot.business.entity.HazardUnitEventRecord;
import io.geekidea.boot.business.query.HazardUnitEventRecordQuery;
import io.geekidea.boot.business.vo.HazardUnitEventRecordVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 安全隐患事件处理记录 Mapper 接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Mapper
public interface HazardUnitEventRecordMapper extends BaseMapper<HazardUnitEventRecord> {

    /**
     * 安全隐患事件处理记录详情
     *
     * @param id
     * @return
     */
    HazardUnitEventRecordVo getHazardUnitEventRecordById(Long id);

    /**
     * 安全隐患事件处理记录分页列表
     *
     * @param query
     * @return
     */
    List<HazardUnitEventRecordVo> getHazardUnitEventRecordPage(HazardUnitEventRecordQuery query);

}
