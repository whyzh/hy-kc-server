package io.geekidea.boot.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.boot.business.entity.HazardUnit;
import io.geekidea.boot.business.query.HazardUnitQuery;
import io.geekidea.boot.business.vo.HazardUnitVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 设备安全隐患部件列 Mapper 接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Mapper
public interface HazardUnitMapper extends BaseMapper<HazardUnit> {

    /**
     * 设备安全隐患部件列详情
     *
     * @param id
     * @return
     */
    HazardUnitVo getHazardUnitById(Long id);

    /**
     * 设备安全隐患部件列分页列表
     *
     * @param query
     * @return
     */
    List<HazardUnitVo> getHazardUnitPage(HazardUnitQuery query);

}
