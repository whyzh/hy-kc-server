package io.geekidea.boot.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.boot.business.mongodb.document.HazardUnitEvent;
import io.geekidea.boot.business.query.HazardUnitEventQuery;
import io.geekidea.boot.business.vo.HazardUnitEventVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 设备部件存在安全隐患事件关联关系 Mapper 接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Mapper
public interface HazardUnitEventMapper extends BaseMapper<HazardUnitEvent> {

    /**
     * 设备部件存在安全隐患事件关联关系详情
     *
     * @param id
     * @return
     */
    HazardUnitEventVo getHazardUnitEventById(Long id);

    /**
     * 设备部件存在安全隐患事件关联关系分页列表
     *
     * @param query
     * @return
     */
    List<HazardUnitEventVo> getHazardUnitEventPage(HazardUnitEventQuery query);

}
