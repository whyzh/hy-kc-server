package io.geekidea.boot.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.boot.business.entity.CntrolMeasure;
import io.geekidea.boot.business.query.CntrolMeasureQuery;
import io.geekidea.boot.business.vo.CntrolMeasureVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 安全管控措施方案 Mapper 接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Mapper
public interface CntrolMeasureMapper extends BaseMapper<CntrolMeasure> {

    /**
     * 安全管控措施方案详情
     *
     * @param id
     * @return
     */
    CntrolMeasureVo getCntrolMeasureById(Long id);

    /**
     * 安全管控措施方案分页列表
     *
     * @param query
     * @return
     */
    List<CntrolMeasureVo> getCntrolMeasurePage(CntrolMeasureQuery query);

}
