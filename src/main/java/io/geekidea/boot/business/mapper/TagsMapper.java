package io.geekidea.boot.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.boot.business.entity.Tags;
import io.geekidea.boot.business.query.TagsQuery;
import io.geekidea.boot.business.vo.TagsVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 标签列 Mapper 接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Mapper
public interface TagsMapper extends BaseMapper<Tags> {

    /**
     * 标签列详情
     *
     * @param id
     * @return
     */
    TagsVo getTagsById(Long id);

    /**
     * 标签列分页列表
     *
     * @param query
     * @return
     */
    List<TagsVo> getTagsPage(TagsQuery query);


    List<TagsVo> getTagsLists(TagsQuery query);
}
