package io.geekidea.boot.business.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 设备等级 level
 *
 **/
public enum HazardLevelEnums {

    DIR(1, "一般"),
    MENU(2, "主要"),
    PERMISSION(3, "核心");

    private Integer code;
    private String desc;

    HazardLevelEnums(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    private static final Map<Integer, HazardLevelEnums> map = new HashMap<>();

    static {
        for (HazardLevelEnums type : values()) {
            map.put(type.code, type);
        }
    }

    public static Integer getCode(HazardLevelEnums type) {
        if (type == null) {
            return null;
        }
        return type.code;
    }

    public static HazardLevelEnums get(Integer code) {
        HazardLevelEnums type = map.get(code);
        if (type == null) {
            return null;
        }
        return type;
    }

    public static String getDesc(Integer code) {
        HazardLevelEnums type = get(code);
        if (type == null) {
            return null;
        }
        return type.getDesc();
    }

}
