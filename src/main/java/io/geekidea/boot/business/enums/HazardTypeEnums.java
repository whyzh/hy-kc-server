package io.geekidea.boot.business.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 设备类型
 *
 **/
public enum HazardTypeEnums {

    DIR(1, "设备"),
    MENU(2, "存储设备"),
    PERMISSION(3, "场所");

    private Integer code;
    private String desc;

    HazardTypeEnums(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    private static final Map<Integer, HazardTypeEnums> map = new HashMap<>();

    static {
        for (HazardTypeEnums type : values()) {
            map.put(type.code, type);
        }
    }

    public static Integer getCode(HazardTypeEnums type) {
        if (type == null) {
            return null;
        }
        return type.code;
    }

    public static HazardTypeEnums get(Integer code) {
        HazardTypeEnums type = map.get(code);
        if (type == null) {
            return null;
        }
        return type;
    }

    public static String getDesc(Integer code) {
        HazardTypeEnums type = get(code);
        if (type == null) {
            return null;
        }
        return type.getDesc();
    }

}
