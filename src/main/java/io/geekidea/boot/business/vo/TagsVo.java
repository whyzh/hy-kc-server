package io.geekidea.boot.business.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 标签列查询结果
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "标签列查询结果")
public class TagsVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "介绍描述")
    private String describe;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "删除，0：未删除，1：已删除")
    private Boolean deleted;

}

