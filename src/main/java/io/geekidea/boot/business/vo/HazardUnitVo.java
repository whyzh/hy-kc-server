package io.geekidea.boot.business.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备安全隐患部件列查询结果
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备安全隐患部件列查询结果")
public class HazardUnitVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "设备id")
    private Long hazardId;

    @Schema(description = "部件名称")
    private String name;

    @Schema(description = "部件编码")
    private String unitCode;

    @Schema(description = "生产单位")
    private String manufacturers;

    @Schema(description = "型号")
    private String model;

    @Schema(description = "规格")
    private String specifications;

    @Schema(description = "使用年限")
    private String durableYears;

    @Schema(description = "投入使用时间")
    private Date useTime;

    @Schema(description = "是否正常使用，0：未使用，1：使用中 2-维修中 3-报废")
    private Boolean status;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "删除，0：未删除，1：已删除")
    private Boolean deleted;

}

