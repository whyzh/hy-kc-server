package io.geekidea.boot.business.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备列查询结果
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备列查询结果")
public class HazardVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "设备编码")
    private String code;

    @Schema(description = "类型：1- 设备  2-存储设施  3-场所")
    private Integer type;

    @Schema(description = "设备等级：1- 一般设备，2- 主要设备，3- 核心设备")
    private Integer grade;

    @Schema(description = "使用单位")
    private String building;

    @Schema(description = "介绍描述")
    private String describe;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "状态，0：禁用，1：启用")
    private Integer status;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "删除，0：未删除，1：已删除")
    private Integer deleted;

}

