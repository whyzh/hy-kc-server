package io.geekidea.boot.business.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 安全管控措施方案查询结果
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "安全管控措施方案查询结果")
public class CntrolMeasureVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "管控方案名")
    private String name;

    @Schema(description = "标签, 多个标签用逗号分隔")
    private String tags;

    @Schema(description = "检查周期(天)")
    private Integer cycle;

    @Schema(description = "介绍描述")
    private String describe;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "删除，0：未删除，1：已删除")
    private Boolean deleted;

}

