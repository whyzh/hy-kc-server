package io.geekidea.boot.business.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备部件存在安全隐患事件列查询结果
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备部件存在安全隐患事件列查询结果")
public class SecureEventVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "风险事件等级：1- 严重，2- 重要，3- 一般，4- 轻微")
    private Integer type;

    @Schema(description = "隐患事项名")
    private String name;

    @Schema(description = "标签, 多个标签用逗号分隔")
    private String tags;

    @Schema(description = "介绍描述")
    private String describe;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "删除，0：未删除，1：已删除")
    private Boolean deleted;

}

