package io.geekidea.boot.business.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备部件存在安全隐患事件关联关系查询结果
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备部件存在安全隐患事件关联关系查询结果")
public class HazardUnitEventVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "设备id")
    private Long hazardId;

    @Schema(description = "部件id")
    private Long unitId;

    @Schema(description = "安全隐患id")
    private Long eventId;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "删除，0：未删除，1：已删除")
    private Boolean deleted;

}

