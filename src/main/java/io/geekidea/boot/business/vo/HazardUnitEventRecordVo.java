package io.geekidea.boot.business.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 安全隐患事件处理记录查询结果
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "安全隐患事件处理记录查询结果")
public class HazardUnitEventRecordVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "安全隐患id")
    private Long eventId;

    @Schema(description = "检测人员ID")
    private Long detectionUserId;

    @Schema(description = "检测时间")
    private Date detectionTime;

    @Schema(description = "检测方式，1：现场检测，2：拍照，3：录像")
    private Integer wayType;

    @Schema(description = "检测反馈备注")
    private String remark;

    @Schema(description = "是否已经处理，0：待处理，1：已处理")
    private Boolean status;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "删除，0：未删除，1：已删除")
    private Boolean deleted;

}

