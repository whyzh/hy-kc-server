package io.geekidea.boot.business.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.geekidea.boot.framework.entity.BaseDocument;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备部件存在安全隐患事件列
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@TableName("secure_event")
@Schema(description = "设备部件存在安全隐患事件列")
public class SecureEvent extends BaseDocument {

    private static final long serialVersionUID = 1L;

    @Schema(description = "风险事件等级：1- 严重，2- 重要，3- 一般，4- 轻微")
    private Integer type;

    @Schema(description = "隐患事项名")
    private String name;

    @Schema(description = "标签, 多个标签用逗号分隔")
    private String tags;

    @Schema(description = "介绍描述")
    @TableField(value = "`describe`")
    private String describe;

}

