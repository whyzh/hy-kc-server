package io.geekidea.boot.business.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.geekidea.boot.framework.entity.BaseDocument;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 *
 */
@Data
@TableName("hazard")
@Schema(description = "设备列表")
public class Hazard extends BaseDocument {

    private static final long serialVersionUID = 1L;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "设备编码")
    private String code;

    @Schema(description = "类型：1- 设备  2-存储设施  3-场所")
    private Integer type;

    @Schema(description = "设备等级：1- 一般设备，2- 主要设备，3- 核心设备")
    private Integer grade;

    @Schema(description = "介绍描述")
    @TableField(value = "`describe`")
    private String describe;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "状态 1：正常，0：禁用")
    private Integer status;

}

