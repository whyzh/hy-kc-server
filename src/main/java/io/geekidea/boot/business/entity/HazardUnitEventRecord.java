package io.geekidea.boot.business.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.geekidea.boot.framework.entity.BaseDocument;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 安全隐患事件处理记录
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@TableName("hazard_unit_event_record")
@Schema(description = "安全隐患事件处理记录")
public class HazardUnitEventRecord extends BaseDocument {

    private static final long serialVersionUID = 1L;

    @Schema(description = "安全隐患id")
    private Long eventId;

    @Schema(description = "检测人员ID")
    private Long detectionUserId;

    @Schema(description = "检测方式，1：现场检测，2：拍照，3：录像")
    private Integer wayType;

    @Schema(description = "检测时间")
    private Date detectionTime;

    @Schema(description = "检测反馈备注")
    private String remark;

    @Schema(description = "是否已经处理，0：待处理，1：已处理")
    private Boolean status;

}

