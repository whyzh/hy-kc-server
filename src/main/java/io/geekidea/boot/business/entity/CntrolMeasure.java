package io.geekidea.boot.business.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.geekidea.boot.framework.entity.BaseDocument;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 安全管控措施方案
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@TableName("cntrol_measure")
@Schema(description = "安全管控措施方案")
public class CntrolMeasure extends BaseDocument {

    private static final long serialVersionUID = 1L;

    @Schema(description = "管控方案名")
    private String name;

    @Schema(description = "检查周期(天)")
    private Integer cycle;

    @Schema(description = "标签, 多个标签用逗号分隔")
    private String tags;

    @Schema(description = "介绍描述")
    @TableField(value = "`describe`")
    private String describe;
}

