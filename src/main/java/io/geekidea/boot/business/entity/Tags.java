package io.geekidea.boot.business.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.geekidea.boot.framework.entity.BaseDocument;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 *
 */
@Data
@TableName("tags")
@Schema(description = "标签列表")
public class Tags extends BaseDocument {

    private static final long serialVersionUID = 1L;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "介绍描述")
    private String describe;

}

