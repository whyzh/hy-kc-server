package io.geekidea.boot.business.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.geekidea.boot.framework.entity.BaseDocument;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 *
 */
@Data
@TableName("hazard_unit")
@Schema(description = "设备安全隐患部件列表")
public class HazardUnit extends BaseDocument {

    private static final long serialVersionUID = 1L;

    @Schema(description = "部件名称")
    private String name;

    @Schema(description = "部件编码")
    private String unitCode;

    @Schema(description = "设备id")
    private int hazardId;

    @Schema(description = "生产单位")
    private String manufacturers;

    @Schema(description = "型号")
    private String model;

    @Schema(description = "规格")
    private String specifications;

    @Schema(description = "使用年限")
    private String durableYears;

    @Schema(description = "是否正常使用，0：未使用，1：使用中 2-维修中 3-报废")
    private int status;

    @Schema(description = "投入使用时间")
    private Date useTime;


}

