package io.geekidea.boot.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.boot.business.entity.HazardUnit;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.HazardUnitDto;
import io.geekidea.boot.business.query.HazardUnitQuery;
import io.geekidea.boot.business.vo.HazardUnitVo;

/**
 * 设备安全隐患部件列 服务接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
public interface HazardUnitService extends IService<HazardUnit> {

    /**
     * 添加设备安全隐患部件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean addHazardUnit(HazardUnitDto dto);

    /**
     * 修改设备安全隐患部件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean updateHazardUnit(HazardUnitDto dto);

    /**
     * 删除设备安全隐患部件列
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteHazardUnit(Long id);

    /**
     * 设备安全隐患部件列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    HazardUnitVo getHazardUnitById(Long id);

    /**
     * 设备安全隐患部件列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    Paging<HazardUnitVo> getHazardUnitPage(HazardUnitQuery query);


}
