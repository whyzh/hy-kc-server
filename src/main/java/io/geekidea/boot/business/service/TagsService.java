package io.geekidea.boot.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.boot.business.entity.Tags;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.TagsDto;
import io.geekidea.boot.business.query.TagsQuery;
import io.geekidea.boot.business.vo.TagsVo;

import java.util.List;


/**
 * 标签列 服务接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
public interface TagsService extends IService<Tags> {

    /**
     * 添加标签列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean addTags(TagsDto dto);

    /**
     * 修改标签列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean updateTags(TagsDto dto);

    /**
     * 删除标签列
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteTags(Long id);

    /**
     * 标签列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    TagsVo getTagsById(Long id);

    /**
     * 标签列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    Paging<TagsVo> getTagsPage(TagsQuery query);

    List<TagsVo> getTagsList(TagsQuery query);
}
