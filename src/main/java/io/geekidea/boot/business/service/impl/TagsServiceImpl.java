package io.geekidea.boot.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.geekidea.boot.business.entity.Tags;
import io.geekidea.boot.framework.exception.BusinessException;
import io.geekidea.boot.framework.page.OrderByItem;
import io.geekidea.boot.framework.page.OrderMapping;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.TagsDto;
import io.geekidea.boot.business.mapper.TagsMapper;
import io.geekidea.boot.business.query.TagsQuery;
import io.geekidea.boot.business.service.TagsService;
import io.geekidea.boot.business.vo.TagsVo;
import io.geekidea.boot.util.PagingUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * 标签列 服务实现类
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@Service
public class TagsServiceImpl extends ServiceImpl<TagsMapper, Tags> implements TagsService {

    @Autowired
    private TagsMapper tagsMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addTags(TagsDto dto) {
        Tags tags = new Tags();
        BeanUtils.copyProperties(dto, tags);
        return save(tags);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateTags(TagsDto dto) {
        Long id = dto.getId();
        Tags tags = getById(id);
        if (tags == null) {
            throw new BusinessException("标签列不存在");
        }
        BeanUtils.copyProperties(dto, tags);
        return updateById(tags);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteTags(Long id) {
        return removeById(id);
    }

    @Override
    public TagsVo getTagsById(Long id) {
        return tagsMapper.getTagsById(id);
    }

    @Override
    public Paging<TagsVo> getTagsPage(TagsQuery query) {
        OrderMapping orderMapping = new OrderMapping();
        orderMapping.put("createTime", "create_time");
        PagingUtil.handlePage(query, orderMapping, OrderByItem.desc("id"));
        List<TagsVo> list = tagsMapper.getTagsPage(query);
        Paging<TagsVo> paging = new Paging<>(list);
        return paging;
    }

    @Override
    public List<TagsVo> getTagsList(TagsQuery query) {
        //查询列表， --todo 如果数据多的时候在做其他处理
        List<TagsVo> list = tagsMapper.getTagsLists(query);
        return list;
    }

}
