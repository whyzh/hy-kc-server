package io.geekidea.boot.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.boot.business.entity.Hazard;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.HazardDto;
import io.geekidea.boot.business.query.HazardQuery;
import io.geekidea.boot.business.vo.HazardVo;

import java.util.List;


/**
 * 设备列 服务接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
public interface HazardService extends IService<Hazard> {

    /**
     * 添加设备列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean addHazard(HazardDto dto);

    /**
     * 修改设备列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean updateHazard(HazardDto dto);

    /**
     * 删除设备列
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteHazard(Long id);

    /**
     * 设备列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    HazardVo getHazardById(Long id);

    /**
     * 设备列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    Paging<HazardVo> getHazardPage(HazardQuery query);
    List<HazardVo> getHazardList(HazardQuery query);

}
