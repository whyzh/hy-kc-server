package io.geekidea.boot.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.geekidea.boot.framework.exception.BusinessException;
import io.geekidea.boot.framework.page.OrderByItem;
import io.geekidea.boot.framework.page.OrderMapping;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.SecureEventDto;
import io.geekidea.boot.business.entity.SecureEvent;
import io.geekidea.boot.business.mapper.SecureEventMapper;
import io.geekidea.boot.business.query.SecureEventQuery;
import io.geekidea.boot.business.service.SecureEventService;
import io.geekidea.boot.business.vo.SecureEventVo;
import io.geekidea.boot.util.PagingUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * 设备部件存在安全隐患事件列 服务实现类
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@Service
public class SecureEventServiceImpl extends ServiceImpl<SecureEventMapper, SecureEvent> implements SecureEventService {

    @Autowired
    private SecureEventMapper secureEventMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addSecureEvent(SecureEventDto dto) {
        SecureEvent secureEvent = new SecureEvent();
        BeanUtils.copyProperties(dto, secureEvent);
        return save(secureEvent);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateSecureEvent(SecureEventDto dto) {
        Long id = dto.getId();
        SecureEvent secureEvent = getById(id);
        if (secureEvent == null) {
            throw new BusinessException("设备部件存在安全隐患事件列不存在");
        }
        BeanUtils.copyProperties(dto, secureEvent);
        return updateById(secureEvent);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteSecureEvent(Long id) {
        return removeById(id);
    }

    @Override
    public SecureEventVo getSecureEventById(Long id) {
        return secureEventMapper.getSecureEventById(id);
    }

    @Override
    public Paging<SecureEventVo> getSecureEventPage(SecureEventQuery query) {
        OrderMapping orderMapping = new OrderMapping();
        orderMapping.put("createTime", "create_time");
        PagingUtil.handlePage(query, orderMapping, OrderByItem.desc("id"));
        List<SecureEventVo> list = secureEventMapper.getSecureEventPage(query);
        Paging<SecureEventVo> paging = new Paging<>(list);
        return paging;
    }

}
