package io.geekidea.boot.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.SecureEventDto;
import io.geekidea.boot.business.entity.SecureEvent;
import io.geekidea.boot.business.query.SecureEventQuery;
import io.geekidea.boot.business.vo.SecureEventVo;


/**
 * 设备部件存在安全隐患事件列 服务接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
public interface SecureEventService extends IService<SecureEvent> {

    /**
     * 添加设备部件存在安全隐患事件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean addSecureEvent(SecureEventDto dto);

    /**
     * 修改设备部件存在安全隐患事件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean updateSecureEvent(SecureEventDto dto);

    /**
     * 删除设备部件存在安全隐患事件列
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteSecureEvent(Long id);

    /**
     * 设备部件存在安全隐患事件列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    SecureEventVo getSecureEventById(Long id);

    /**
     * 设备部件存在安全隐患事件列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    Paging<SecureEventVo> getSecureEventPage(SecureEventQuery query);


}
