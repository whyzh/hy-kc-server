package io.geekidea.boot.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.HazardUnitEventDto;
import io.geekidea.boot.business.mongodb.document.HazardUnitEvent;
import io.geekidea.boot.business.query.HazardUnitEventQuery;
import io.geekidea.boot.business.vo.HazardUnitEventVo;


/**
 * 设备部件存在安全隐患事件关联关系 服务接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
public interface HazardUnitEventService extends IService<HazardUnitEvent> {

    /**
     * 添加设备部件存在安全隐患事件关联关系
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean addHazardUnitEvent(HazardUnitEventDto dto);

    /**
     * 修改设备部件存在安全隐患事件关联关系
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean updateHazardUnitEvent(HazardUnitEventDto dto);

    /**
     * 删除设备部件存在安全隐患事件关联关系
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteHazardUnitEvent(Long id);

    /**
     * 设备部件存在安全隐患事件关联关系详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    HazardUnitEventVo getHazardUnitEventById(Long id);

    /**
     * 设备部件存在安全隐患事件关联关系分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    Paging<HazardUnitEventVo> getHazardUnitEventPage(HazardUnitEventQuery query);

}
