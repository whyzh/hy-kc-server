package io.geekidea.boot.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.CntrolMeasureDto;
import io.geekidea.boot.business.entity.CntrolMeasure;
import io.geekidea.boot.business.query.CntrolMeasureQuery;
import io.geekidea.boot.business.vo.CntrolMeasureVo;

/**
 * 安全管控措施方案 服务接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
public interface CntrolMeasureService extends IService<CntrolMeasure> {

    /**
     * 添加安全管控措施方案
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean addCntrolMeasure(CntrolMeasureDto dto);

    /**
     * 修改安全管控措施方案
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean updateCntrolMeasure(CntrolMeasureDto dto);

    /**
     * 删除安全管控措施方案
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteCntrolMeasure(Long id);

    /**
     * 安全管控措施方案详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    CntrolMeasureVo getCntrolMeasureById(Long id);

    /**
     * 安全管控措施方案分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    Paging<CntrolMeasureVo> getCntrolMeasurePage(CntrolMeasureQuery query);

}
