package io.geekidea.boot.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.geekidea.boot.framework.exception.BusinessException;
import io.geekidea.boot.framework.page.OrderByItem;
import io.geekidea.boot.framework.page.OrderMapping;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.HazardUnitEventRecordDto;
import io.geekidea.boot.business.entity.HazardUnitEventRecord;
import io.geekidea.boot.business.mapper.HazardUnitEventRecordMapper;
import io.geekidea.boot.business.query.HazardUnitEventRecordQuery;
import io.geekidea.boot.business.service.HazardUnitEventRecordService;
import io.geekidea.boot.business.vo.HazardUnitEventRecordVo;
import io.geekidea.boot.util.PagingUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * 安全隐患事件处理记录 服务实现类
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@Service
public class HazardUnitEventRecordServiceImpl extends ServiceImpl<HazardUnitEventRecordMapper, HazardUnitEventRecord> implements HazardUnitEventRecordService {

    @Autowired
    private HazardUnitEventRecordMapper hazardUnitEventRecordMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addHazardUnitEventRecord(HazardUnitEventRecordDto dto) {
        HazardUnitEventRecord hazardUnitEventRecord = new HazardUnitEventRecord();
        BeanUtils.copyProperties(dto, hazardUnitEventRecord);
        return save(hazardUnitEventRecord);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateHazardUnitEventRecord(HazardUnitEventRecordDto dto) {
        Long id = dto.getId();
        HazardUnitEventRecord hazardUnitEventRecord = getById(id);
        if (hazardUnitEventRecord == null) {
            throw new BusinessException("安全隐患事件处理记录不存在");
        }
        BeanUtils.copyProperties(dto, hazardUnitEventRecord);
        return updateById(hazardUnitEventRecord);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteHazardUnitEventRecord(Long id) {
        return removeById(id);
    }

    @Override
    public HazardUnitEventRecordVo getHazardUnitEventRecordById(Long id) {
        return hazardUnitEventRecordMapper.getHazardUnitEventRecordById(id);
    }

    @Override
    public Paging<HazardUnitEventRecordVo> getHazardUnitEventRecordPage(HazardUnitEventRecordQuery query) {
        OrderMapping orderMapping = new OrderMapping();
        orderMapping.put("createTime", "create_time");
        PagingUtil.handlePage(query, orderMapping, OrderByItem.desc("id"));
        List<HazardUnitEventRecordVo> list = hazardUnitEventRecordMapper.getHazardUnitEventRecordPage(query);
        Paging<HazardUnitEventRecordVo> paging = new Paging<>(list);
        return paging;
    }
}
