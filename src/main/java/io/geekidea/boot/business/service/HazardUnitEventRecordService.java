package io.geekidea.boot.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.business.dto.HazardUnitEventRecordDto;
import io.geekidea.boot.business.entity.HazardUnitEventRecord;
import io.geekidea.boot.business.query.HazardUnitEventRecordQuery;
import io.geekidea.boot.business.vo.HazardUnitEventRecordVo;


/**
 * 安全隐患事件处理记录 服务接口
 *
 * @author mr.wei
 * @since 2024-04-10
 */
public interface HazardUnitEventRecordService extends IService<HazardUnitEventRecord> {

    /**
     * 添加安全隐患事件处理记录
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean addHazardUnitEventRecord(HazardUnitEventRecordDto dto);

    /**
     * 修改安全隐患事件处理记录
     *
     * @param dto
     * @return
     * @throws Exception
     */
    boolean updateHazardUnitEventRecord(HazardUnitEventRecordDto dto);

    /**
     * 删除安全隐患事件处理记录
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteHazardUnitEventRecord(Long id);

    /**
     * 安全隐患事件处理记录详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    HazardUnitEventRecordVo getHazardUnitEventRecordById(Long id);

    /**
     * 安全隐患事件处理记录分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    Paging<HazardUnitEventRecordVo> getHazardUnitEventRecordPage(HazardUnitEventRecordQuery query);

}
