package io.geekidea.boot.business.mongodb.repository;


import io.geekidea.boot.business.mongodb.document.HazardUnitEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 设备部件存在安全隐患事件关联关系
 */
public interface HazardUnitEventRepository extends MongoRepository<HazardUnitEvent, String> {

}
