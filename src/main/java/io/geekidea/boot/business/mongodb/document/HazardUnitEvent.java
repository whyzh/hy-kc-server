package io.geekidea.boot.business.mongodb.document;

import io.geekidea.boot.framework.entity.BaseDocument;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * 设备部件存在安全隐患事件关联关系
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "hazard_unit_events")
@TypeAlias("hazard_unit_events")
@Schema(description = "设备部件存在安全隐患事件关联关系")
public class HazardUnitEvent extends BaseDocument {

    private static final long serialVersionUID = 1L;

    @Schema(description = "设备id")
    private Long hazardId;

    @Schema(description = "部件id")
    private List<Long> unitIds;

    @Schema(description = "安全隐患事件id")
    private List<Long> eventIds;

    @Schema(description = "处理措施id")
    private List<Long> cntrolId;

}

