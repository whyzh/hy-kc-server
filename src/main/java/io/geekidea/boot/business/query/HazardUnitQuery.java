package io.geekidea.boot.business.query;

import io.geekidea.boot.framework.page.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * 设备安全隐患部件列查询参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备安全隐患部件列查询参数")
public class HazardUnitQuery extends BasePageQuery {

    private static final long serialVersionUID = 1L;

}

