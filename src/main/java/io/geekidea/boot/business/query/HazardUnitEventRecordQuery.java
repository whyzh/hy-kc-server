package io.geekidea.boot.business.query;

import io.geekidea.boot.framework.page.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * 安全隐患事件处理记录查询参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "安全隐患事件处理记录查询参数")
public class HazardUnitEventRecordQuery extends BasePageQuery {

    private static final long serialVersionUID = 1L;

}

