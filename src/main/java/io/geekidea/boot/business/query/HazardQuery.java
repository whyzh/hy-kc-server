package io.geekidea.boot.business.query;

import io.geekidea.boot.framework.page.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * 设备列查询参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备列查询参数")
public class HazardQuery extends BasePageQuery {

    private static final long serialVersionUID = 1L;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "状态，0：禁用，1：启用")
    private Integer status;

}

