package io.geekidea.boot.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 安全管控措施方案参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "安全管控措施方案参数")
public class CntrolMeasureDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "管控方案名")
    @NotBlank(message = "管控方案名不能为空")
    @Length(max = 20, message = "管控方案名长度超过限制")
    private String name;

    @Schema(description = "检查周期(天)")
    @NotNull(message = "检查周期不能为空")
    private Integer cycle;

    @Schema(description = "标签, 多个标签用逗号分隔")
    private String tags;

    @Schema(description = "介绍描述")
    private String describe;

}


