package io.geekidea.boot.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 设备安全隐患部件列参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备安全隐患部件列参数")
public class HazardUnitDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "设备id")
    @NotNull(message = "设备id不能为空")
    private Long hazardId;

    @Schema(description = "部件名称")
    @NotBlank(message = "部件名称不能为空")
    @Length(max = 20, message = "部件名称长度超过限制")
    private String name;

    @Schema(description = "部件编码")
    @NotBlank(message = "部件编码不能为空")
    @Length(max = 50, message = "部件编码长度超过限制")
    private String unitCode;

    @Schema(description = "生产单位")
    private String manufacturers;

    @Schema(description = "型号")
    private String model;

    @Schema(description = "规格")
    private String specifications;

    @Schema(description = "使用年限")
    private String durableYears;

    @Schema(description = "投入使用时间")
    private Date useTime;

    @Schema(description = "是否正常使用，0：未使用，1：使用中 2-维修中 3-报废")
    private Integer status;

}


