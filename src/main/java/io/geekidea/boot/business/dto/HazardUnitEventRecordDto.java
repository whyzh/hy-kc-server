package io.geekidea.boot.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 安全隐患事件处理记录参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "安全隐患事件处理记录参数")
public class HazardUnitEventRecordDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "安全隐患id")
    @NotNull(message = "安全隐患id不能为空")
    private Long eventId;

    @Schema(description = "检测人员ID")
    private Long detectionUserId;

    @Schema(description = "检测方式，1：现场检测，2：拍照，3：录像")
    @NotNull(message = "检测方式不能为空")
    private Integer wayType;

    @Schema(description = "检测时间")
    private Date detectionTime;

    @Schema(description = "检测反馈备注")
    private String remark;

    @Schema(description = "是否已经处理，0：待处理，1：已处理")
    private Integer status;

}


