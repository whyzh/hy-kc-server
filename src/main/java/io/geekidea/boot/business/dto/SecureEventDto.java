package io.geekidea.boot.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 设备部件存在安全隐患事件列参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备部件存在安全隐患事件列参数")
public class SecureEventDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "风险事件等级：1- 严重，2- 重要，3- 一般，4- 轻微")
    @NotNull(message = "风险事件等级：1-不能为空")
    private Integer type;

    @Schema(description = "隐患事项名")
    @NotBlank(message = "隐患事项名不能为空")
    @Length(max = 20, message = "隐患事项名长度超过限制")
    private String name;

    @Schema(description = "标签, 多个标签用逗号分隔")
    private String tags;

    @Schema(description = "介绍描述")
    private String describe;

}


