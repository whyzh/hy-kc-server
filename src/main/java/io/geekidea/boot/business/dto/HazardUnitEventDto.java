package io.geekidea.boot.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 设备部件存在安全隐患事件关联关系参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备部件存在安全隐患事件关联关系参数")
public class HazardUnitEventDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "设备id")
    @NotNull(message = "设备id不能为空")
    private Long hazardId;

    @Schema(description = "部件id")
    @NotNull(message = "部件id不能为空")
    private Long unitId;

    @Schema(description = "安全隐患id")
    @NotNull(message = "安全隐患id不能为空")
    private Long eventId;

}


