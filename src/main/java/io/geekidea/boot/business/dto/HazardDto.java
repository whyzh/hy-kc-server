package io.geekidea.boot.business.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 设备列参数
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Data
@Schema(description = "设备列参数")
public class HazardDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    private Long id;

    @Schema(description = "名称")
    @NotBlank(message = "名称不能为空")
    @Length(max = 20, message = "名称长度超过限制")
    private String name;

    @Schema(description = "设备编码")
    @NotBlank(message = "设备编码不能为空")
    @Length(max = 50, message = "设备编码长度超过限制")
    private String code;

    @Schema(description = "类型：1- 设备  2-存储设施  3-场所")
    @NotNull(message = "类型：1-不能为空")
    private Integer type;

    @Schema(description = "设备等级：1- 一般设备，2- 主要设备，3- 核心设备")
    private Integer grade;

    @Schema(description = "使用单位")
    private String building;

    @Schema(description = "介绍描述")
    private String describe;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "状态，0：禁用，1：启用")
    private Integer status;

}


