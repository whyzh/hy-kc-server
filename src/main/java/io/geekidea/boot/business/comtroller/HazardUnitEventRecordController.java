package io.geekidea.boot.business.comtroller;

import io.geekidea.boot.auth.annotation.Permission;
import io.geekidea.boot.common.enums.SysLogType;
import io.geekidea.boot.framework.annotation.Log;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.framework.response.ApiResult;
import io.geekidea.boot.business.dto.HazardUnitEventRecordDto;
import io.geekidea.boot.business.query.HazardUnitEventRecordQuery;
import io.geekidea.boot.business.service.HazardUnitEventRecordService;
import io.geekidea.boot.business.vo.HazardUnitEventRecordVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 安全隐患事件处理记录 控制器
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@RestController
@Tag(name = "安全隐患事件处理记录")
@RequestMapping("/admin/hazardUnitEventRecord")
public class HazardUnitEventRecordController {

    @Autowired
    private HazardUnitEventRecordService hazardUnitEventRecordService;

    /**
     * 添加安全隐患事件处理记录
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.ADD)
    @Operation(summary = "添加安全隐患事件处理记录")
    @PostMapping("/addHazardUnitEventRecord")
    @Permission("hazard:unit:event:record:add")
    public ApiResult addHazardUnitEventRecord(@Valid @RequestBody HazardUnitEventRecordDto dto) {
        log.info("添加安全隐患事件处理记录：{}", dto);
        boolean flag = hazardUnitEventRecordService.addHazardUnitEventRecord(dto);
        return ApiResult.result(flag);
    }

    /**
     * 修改安全隐患事件处理记录
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.UPDATE)
    @Operation(summary = "修改安全隐患事件处理记录")
    @PostMapping("/updateHazardUnitEventRecord")
    @Permission("hazard:unit:event:record:update")
    public ApiResult updateHazardUnitEventRecord(@Valid @RequestBody HazardUnitEventRecordDto dto) {
        log.info("修改安全隐患事件处理记录：{}", dto);
        boolean flag = hazardUnitEventRecordService.updateHazardUnitEventRecord(dto);
        return ApiResult.result(flag);
    }

    /**
     * 删除安全隐患事件处理记录
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.DELETE)
    @Operation(summary = "删除安全隐患事件处理记录")
    @PostMapping("/deleteHazardUnitEventRecord/{id}")
    @Permission("hazard:unit:event:record:delete")
    public ApiResult deleteHazardUnitEventRecord(@PathVariable Long id) {
        log.info("删除安全隐患事件处理记录：{}", id);
        boolean flag = hazardUnitEventRecordService.deleteHazardUnitEventRecord(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取安全隐患事件处理记录详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取安全隐患事件处理记录详情")
    @PostMapping("/getHazardUnitEventRecord/{id}")
    @Permission("hazard:unit:event:record:info")
    public ApiResult<HazardUnitEventRecordVo> getHazardUnitEventRecord(@PathVariable Long id) {
        log.info("获取安全隐患事件处理记录详情：{}", id);
        HazardUnitEventRecordVo hazardUnitEventRecordVo = hazardUnitEventRecordService.getHazardUnitEventRecordById(id);
        return ApiResult.success(hazardUnitEventRecordVo);
    }

    /**
     * 获取安全隐患事件处理记录分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取安全隐患事件处理记录分页列表")
    @PostMapping("/getHazardUnitEventRecordPage")
    @Permission("hazard:unit:event:record:page")
    public ApiResult<HazardUnitEventRecordVo> getHazardUnitEventRecordPage(@Valid @RequestBody HazardUnitEventRecordQuery query) {
        log.info("获取安全隐患事件处理记录分页列表：{}", query);
        Paging<HazardUnitEventRecordVo> paging = hazardUnitEventRecordService.getHazardUnitEventRecordPage(query);
        return ApiResult.success(paging);
    }

}
