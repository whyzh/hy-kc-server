package io.geekidea.boot.business.comtroller;

import io.geekidea.boot.auth.annotation.Permission;
import io.geekidea.boot.common.enums.SysLogType;
import io.geekidea.boot.framework.annotation.Log;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.framework.response.ApiResult;
import io.geekidea.boot.business.dto.HazardUnitDto;
import io.geekidea.boot.business.query.HazardUnitQuery;
import io.geekidea.boot.business.service.HazardUnitService;
import io.geekidea.boot.business.vo.HazardUnitVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 设备安全隐患部件列 控制器
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@RestController
@Tag(name = "设备安全隐患部件列")
@RequestMapping("/admin/hazardUnit")
public class HazardUnitController {

    @Autowired
    private HazardUnitService hazardUnitService;

    /**
     * 添加设备安全隐患部件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.ADD)
    @Operation(summary = "添加设备安全隐患部件列")
    @PostMapping("/addHazardUnit")
    @Permission("hazard:unit:add")
    public ApiResult addHazardUnit(@Valid @RequestBody HazardUnitDto dto) {
        log.info("添加设备安全隐患部件列：{}", dto);
        boolean flag = hazardUnitService.addHazardUnit(dto);
        return ApiResult.result(flag);
    }

    /**
     * 修改设备安全隐患部件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.UPDATE)
    @Operation(summary = "修改设备安全隐患部件列")
    @PostMapping("/updateHazardUnit")
    @Permission("hazard:unit:update")
    public ApiResult updateHazardUnit(@Valid @RequestBody HazardUnitDto dto) {
        log.info("修改设备安全隐患部件列：{}", dto);
        boolean flag = hazardUnitService.updateHazardUnit(dto);
        return ApiResult.result(flag);
    }

    /**
     * 删除设备安全隐患部件列
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.DELETE)
    @Operation(summary = "删除设备安全隐患部件列")
    @PostMapping("/deleteHazardUnit/{id}")
    @Permission("hazard:unit:delete")
    public ApiResult deleteHazardUnit(@PathVariable Long id) {
        log.info("删除设备安全隐患部件列：{}", id);
        boolean flag = hazardUnitService.deleteHazardUnit(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取设备安全隐患部件列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备安全隐患部件列详情")
    @PostMapping("/getHazardUnit/{id}")
    @Permission("hazard:unit:info")
    public ApiResult<HazardUnitVo> getHazardUnit(@PathVariable Long id) {
        log.info("获取设备安全隐患部件列详情：{}", id);
        HazardUnitVo hazardUnitVo = hazardUnitService.getHazardUnitById(id);
        return ApiResult.success(hazardUnitVo);
    }

    /**
     * 获取设备安全隐患部件列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备安全隐患部件列分页列表")
    @PostMapping("/getHazardUnitPage")
    @Permission("hazard:unit:page")
    public ApiResult<HazardUnitVo> getHazardUnitPage(@Valid @RequestBody HazardUnitQuery query) {
        log.info("获取设备安全隐患部件列分页列表：{}", query);
        Paging<HazardUnitVo> paging = hazardUnitService.getHazardUnitPage(query);
        return ApiResult.success(paging);
    }

}
