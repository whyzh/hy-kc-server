package io.geekidea.boot.business.comtroller;

import io.geekidea.boot.auth.annotation.Permission;
import io.geekidea.boot.common.enums.SysLogType;
import io.geekidea.boot.framework.annotation.Log;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.framework.response.ApiResult;
import io.geekidea.boot.business.dto.SecureEventDto;
import io.geekidea.boot.business.query.SecureEventQuery;
import io.geekidea.boot.business.service.SecureEventService;
import io.geekidea.boot.business.vo.SecureEventVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 设备部件存在安全隐患事件列 控制器
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@RestController
@Tag(name = "设备部件存在安全隐患事件列")
@RequestMapping("/admin/secureEvent")
public class SecureEventController {

    @Autowired
    private SecureEventService secureEventService;

    /**
     * 添加设备部件存在安全隐患事件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.ADD)
    @Operation(summary = "添加设备部件存在安全隐患事件列")
    @PostMapping("/addSecureEvent")
    @Permission("secure:event:add")
    public ApiResult addSecureEvent(@Valid @RequestBody SecureEventDto dto) {
        log.info("添加设备部件存在安全隐患事件列：{}", dto);
        boolean flag = secureEventService.addSecureEvent(dto);
        return ApiResult.result(flag);
    }

    /**
     * 修改设备部件存在安全隐患事件列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.UPDATE)
    @Operation(summary = "修改设备部件存在安全隐患事件列")
    @PostMapping("/updateSecureEvent")
    @Permission("secure:event:update")
    public ApiResult updateSecureEvent(@Valid @RequestBody SecureEventDto dto) {
        log.info("修改设备部件存在安全隐患事件列：{}", dto);
        boolean flag = secureEventService.updateSecureEvent(dto);
        return ApiResult.result(flag);
    }

    /**
     * 删除设备部件存在安全隐患事件列
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.DELETE)
    @Operation(summary = "删除设备部件存在安全隐患事件列")
    @PostMapping("/deleteSecureEvent/{id}")
    @Permission("secure:event:delete")
    public ApiResult deleteSecureEvent(@PathVariable Long id) {
        log.info("删除设备部件存在安全隐患事件列：{}", id);
        boolean flag = secureEventService.deleteSecureEvent(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取设备部件存在安全隐患事件列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备部件存在安全隐患事件列详情")
    @PostMapping("/getSecureEvent/{id}")
    @Permission("secure:event:info")
    public ApiResult<SecureEventVo> getSecureEvent(@PathVariable Long id) {
        log.info("获取设备部件存在安全隐患事件列详情：{}", id);
        SecureEventVo secureEventVo = secureEventService.getSecureEventById(id);
        return ApiResult.success(secureEventVo);
    }

    /**
     * 获取设备部件存在安全隐患事件列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备部件存在安全隐患事件列分页列表")
    @PostMapping("/getSecureEventPage")
    @Permission("secure:event:page")
    public ApiResult<SecureEventVo> getSecureEventPage(@Valid @RequestBody SecureEventQuery query) {
        log.info("获取设备部件存在安全隐患事件列分页列表：{}", query);
        Paging<SecureEventVo> paging = secureEventService.getSecureEventPage(query);
        return ApiResult.success(paging);
    }

}
