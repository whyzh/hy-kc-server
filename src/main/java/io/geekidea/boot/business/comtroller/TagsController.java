package io.geekidea.boot.business.comtroller;

import io.geekidea.boot.auth.annotation.Permission;
import io.geekidea.boot.common.enums.SysLogType;
import io.geekidea.boot.framework.annotation.Log;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.framework.response.ApiResult;
import io.geekidea.boot.business.dto.TagsDto;
import io.geekidea.boot.business.query.TagsQuery;
import io.geekidea.boot.business.service.TagsService;
import io.geekidea.boot.business.vo.TagsVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 标签列 控制器
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@RestController
@Tag(name = "标签列")
@RequestMapping("/admin/tags")
public class TagsController {

    @Autowired
    private TagsService tagsService;

    /**
     * 添加标签列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.ADD)
    @Operation(summary = "添加标签列")
    @PostMapping("/addTags")
    @Permission("tags:add")
    public ApiResult addTags(@Valid @RequestBody TagsDto dto) {
        log.info("添加标签列：{}", dto);
        boolean flag = tagsService.addTags(dto);
        return ApiResult.result(flag);
    }

    /**
     * 修改标签列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.UPDATE)
    @Operation(summary = "修改标签列")
    @PostMapping("/updateTags")
    @Permission("tags:update")
    public ApiResult updateTags(@Valid @RequestBody TagsDto dto) {
        log.info("修改标签列：{}", dto);
        boolean flag = tagsService.updateTags(dto);
        return ApiResult.result(flag);
    }

    /**
     * 删除标签列
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.DELETE)
    @Operation(summary = "删除标签列")
    @PostMapping("/deleteTags/{id}")
    @Permission("tags:delete")
    public ApiResult deleteTags(@PathVariable Long id) {
        log.info("删除标签列：{}", id);
        boolean flag = tagsService.deleteTags(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取标签列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取标签列详情")
    @PostMapping("/getTags/{id}")
    @Permission("tags:info")
    public ApiResult<TagsVo> getTags(@PathVariable Long id) {
        log.info("获取标签列详情：{}", id);
        TagsVo tagsVo = tagsService.getTagsById(id);
        return ApiResult.success(tagsVo);
    }

    /**
     * 获取标签列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取标签列分页列表")
    @PostMapping("/getTagsPage")
    @Permission("tags:page")
    public ApiResult<TagsVo> getTagsPage(@Valid @RequestBody TagsQuery query) {
        log.info("获取标签列分页列表：{}", query);
        Paging<TagsVo> paging = tagsService.getTagsPage(query);
        return ApiResult.success(paging);
    }

    /**
     * 获取标签列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取标签列分页列表")
    @PostMapping("/getTagsList")
    @Permission("tags:list")
    public ApiResult<List<TagsVo>> getTagsList(@Valid @RequestBody TagsQuery query) {
        log.info("获取标签列分页列表：{}", query);
        List<TagsVo> paging = tagsService.getTagsList(query);
        return ApiResult.success(paging);
    }

}
