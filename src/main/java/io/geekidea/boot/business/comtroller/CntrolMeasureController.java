package io.geekidea.boot.business.comtroller;

import io.geekidea.boot.auth.annotation.Permission;
import io.geekidea.boot.common.enums.SysLogType;
import io.geekidea.boot.framework.annotation.Log;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.framework.response.ApiResult;
import io.geekidea.boot.business.dto.CntrolMeasureDto;
import io.geekidea.boot.business.query.CntrolMeasureQuery;
import io.geekidea.boot.business.service.CntrolMeasureService;
import io.geekidea.boot.business.vo.CntrolMeasureVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 安全管控措施方案 控制器
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@RestController
@Tag(name = "安全管控措施方案")
@RequestMapping("/admin/cntrolMeasure")
public class CntrolMeasureController {

    @Autowired
    private CntrolMeasureService cntrolMeasureService;

    /**
     * 添加安全管控措施方案
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.ADD)
    @Operation(summary = "添加安全管控措施方案")
    @PostMapping("/addCntrolMeasure")
    @Permission("cntrol:measure:add")
    public ApiResult addCntrolMeasure(@Valid @RequestBody CntrolMeasureDto dto) {
        log.info("添加安全管控措施方案：{}", dto);
        boolean flag = cntrolMeasureService.addCntrolMeasure(dto);
        return ApiResult.result(flag);
    }

    /**
     * 修改安全管控措施方案
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.UPDATE)
    @Operation(summary = "修改安全管控措施方案")
    @PostMapping("/updateCntrolMeasure")
    @Permission("cntrol:measure:update")
    public ApiResult updateCntrolMeasure(@Valid @RequestBody CntrolMeasureDto dto) {
        log.info("修改安全管控措施方案：{}", dto);
        boolean flag = cntrolMeasureService.updateCntrolMeasure(dto);
        return ApiResult.result(flag);
    }

    /**
     * 删除安全管控措施方案
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.DELETE)
    @Operation(summary = "删除安全管控措施方案")
    @PostMapping("/deleteCntrolMeasure/{id}")
    @Permission("cntrol:measure:delete")
    public ApiResult deleteCntrolMeasure(@PathVariable Long id) {
        log.info("删除安全管控措施方案：{}", id);
        boolean flag = cntrolMeasureService.deleteCntrolMeasure(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取安全管控措施方案详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取安全管控措施方案详情")
    @PostMapping("/getCntrolMeasure/{id}")
    @Permission("cntrol:measure:info")
    public ApiResult<CntrolMeasureVo> getCntrolMeasure(@PathVariable Long id) {
        log.info("获取安全管控措施方案详情：{}", id);
        CntrolMeasureVo cntrolMeasureVo = cntrolMeasureService.getCntrolMeasureById(id);
        return ApiResult.success(cntrolMeasureVo);
    }

    /**
     * 获取安全管控措施方案分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取安全管控措施方案分页列表")
    @PostMapping("/getCntrolMeasurePage")
    @Permission("cntrol:measure:page")
    public ApiResult<CntrolMeasureVo> getCntrolMeasurePage(@Valid @RequestBody CntrolMeasureQuery query) {
        log.info("获取安全管控措施方案分页列表：{}", query);
        Paging<CntrolMeasureVo> paging = cntrolMeasureService.getCntrolMeasurePage(query);
        return ApiResult.success(paging);
    }

}
