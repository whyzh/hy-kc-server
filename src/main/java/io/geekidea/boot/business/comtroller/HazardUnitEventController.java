package io.geekidea.boot.business.comtroller;

import io.geekidea.boot.auth.annotation.Permission;
import io.geekidea.boot.common.enums.SysLogType;
import io.geekidea.boot.framework.annotation.Log;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.framework.response.ApiResult;
import io.geekidea.boot.business.dto.HazardUnitEventDto;
import io.geekidea.boot.business.query.HazardUnitEventQuery;
import io.geekidea.boot.business.service.HazardUnitEventService;
import io.geekidea.boot.business.vo.HazardUnitEventVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 设备部件存在安全隐患事件关联关系 控制器
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@RestController
@Tag(name = "设备部件存在安全隐患事件关联关系")
@RequestMapping("/admin/hazardUnitEvent")
public class HazardUnitEventController {

    @Autowired
    private HazardUnitEventService hazardUnitEventService;

    /**
     * 添加设备部件存在安全隐患事件关联关系
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.ADD)
    @Operation(summary = "添加设备部件存在安全隐患事件关联关系")
    @PostMapping("/addHazardUnitEvent")
    @Permission("hazard:unit:event:add")
    public ApiResult addHazardUnitEvent(@Valid @RequestBody HazardUnitEventDto dto) {
        log.info("添加设备部件存在安全隐患事件关联关系：{}", dto);
        boolean flag = hazardUnitEventService.addHazardUnitEvent(dto);
        return ApiResult.result(flag);
    }

    /**
     * 修改设备部件存在安全隐患事件关联关系
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.UPDATE)
    @Operation(summary = "修改设备部件存在安全隐患事件关联关系")
    @PostMapping("/updateHazardUnitEvent")
    @Permission("hazard:unit:event:update")
    public ApiResult updateHazardUnitEvent(@Valid @RequestBody HazardUnitEventDto dto) {
        log.info("修改设备部件存在安全隐患事件关联关系：{}", dto);
        boolean flag = hazardUnitEventService.updateHazardUnitEvent(dto);
        return ApiResult.result(flag);
    }

    /**
     * 删除设备部件存在安全隐患事件关联关系
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.DELETE)
    @Operation(summary = "删除设备部件存在安全隐患事件关联关系")
    @PostMapping("/deleteHazardUnitEvent/{id}")
    @Permission("hazard:unit:event:delete")
    public ApiResult deleteHazardUnitEvent(@PathVariable Long id) {
        log.info("删除设备部件存在安全隐患事件关联关系：{}", id);
        boolean flag = hazardUnitEventService.deleteHazardUnitEvent(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取设备部件存在安全隐患事件关联关系详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备部件存在安全隐患事件关联关系详情")
    @PostMapping("/getHazardUnitEvent/{id}")
    @Permission("hazard:unit:event:info")
    public ApiResult<HazardUnitEventVo> getHazardUnitEvent(@PathVariable Long id) {
        log.info("获取设备部件存在安全隐患事件关联关系详情：{}", id);
        HazardUnitEventVo hazardUnitEventVo = hazardUnitEventService.getHazardUnitEventById(id);
        return ApiResult.success(hazardUnitEventVo);
    }

    /**
     * 获取设备部件存在安全隐患事件关联关系分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备部件存在安全隐患事件关联关系分页列表")
    @PostMapping("/getHazardUnitEventPage")
    @Permission("hazard:unit:event:page")
    public ApiResult<HazardUnitEventVo> getHazardUnitEventPage(@Valid @RequestBody HazardUnitEventQuery query) {
        log.info("获取设备部件存在安全隐患事件关联关系分页列表：{}", query);
        Paging<HazardUnitEventVo> paging = hazardUnitEventService.getHazardUnitEventPage(query);
        return ApiResult.success(paging);
    }

}
