package io.geekidea.boot.business.comtroller;

import io.geekidea.boot.auth.annotation.Permission;
import io.geekidea.boot.common.enums.SysLogType;
import io.geekidea.boot.framework.annotation.Log;
import io.geekidea.boot.framework.page.Paging;
import io.geekidea.boot.framework.response.ApiResult;
import io.geekidea.boot.business.dto.HazardDto;
import io.geekidea.boot.business.query.HazardQuery;
import io.geekidea.boot.business.service.HazardService;
import io.geekidea.boot.business.vo.HazardVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 设备列 控制器
 *
 * @author mr.wei
 * @since 2024-04-10
 */
@Slf4j
@RestController
@Tag(name = "设备列")
@RequestMapping("/admin/hazard")
public class HazardController {

    @Autowired
    private HazardService hazardService;

    /**
     * 添加设备列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.ADD)
    @Operation(summary = "添加设备列")
    @PostMapping("/addHazard")
    @Permission("hazard:add")
    public ApiResult addHazard(@Valid @RequestBody HazardDto dto) {
        log.info("添加设备列：{}", dto);
        boolean flag = hazardService.addHazard(dto);
        return ApiResult.result(flag);
    }

    /**
     * 修改设备列
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.UPDATE)
    @Operation(summary = "修改设备列")
    @PostMapping("/updateHazard")
    @Permission("hazard:update")
    public ApiResult updateHazard(@Valid @RequestBody HazardDto dto) {
        log.info("修改设备列：{}", dto);
        boolean flag = hazardService.updateHazard(dto);
        return ApiResult.result(flag);
    }

    /**
     * 删除设备列
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Log(type = SysLogType.DELETE)
    @Operation(summary = "删除设备列")
    @PostMapping("/deleteHazard/{id}")
    @Permission("hazard:delete")
    public ApiResult deleteHazard(@PathVariable Long id) {
        log.info("删除设备列：{}", id);
        boolean flag = hazardService.deleteHazard(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取设备列详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备列详情")
    @PostMapping("/getHazard/{id}")
    @Permission("hazard:info")
    public ApiResult<HazardVo> getHazard(@PathVariable Long id) {
        log.info("获取设备列详情：{}", id);
        HazardVo hazardVo = hazardService.getHazardById(id);
        return ApiResult.success(hazardVo);
    }

    /**
     * 获取设备列分页列表
     *
     * @param query
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取设备列分页列表")
    @PostMapping("/getHazardPage")
    @Permission("hazard:page")
    public ApiResult<HazardVo> getHazardPage(@Valid @RequestBody HazardQuery query) {
        log.info("获取设备列分页列表：{}", query);
        Paging<HazardVo> paging = hazardService.getHazardPage(query);
        return ApiResult.success(paging);
    }

    @Operation(summary = "获取设备列总列表")
    @PostMapping("/getHazardList")
    @Permission("hazard:list")
    public ApiResult<List<HazardVo>> getHazardList(@Valid @RequestBody HazardQuery query) {
        log.info("获取设备列分页列表：{}", query);
        List<HazardVo> paging = hazardService.getHazardList(query);
        return ApiResult.success(paging);
    }

}
