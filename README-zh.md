## 开源协议 MIT-License

### 主要特性
- 集成spring boot 常用开发组件集、公共配置、AOP日志等
- 单体架构，更快更简单
- 提供PC管理端与APP端代码接口
- 集成mybatis plus快速dao操作
- 集成Swagger/Knife4j，可自动生成api文档
- 集成Redis缓存
- 集成HikariCP连接池，JDBC性能和慢查询检测


### 项目环境 
名称 | 版本 |  备注
-|-|-
JDK | 1.8+ | JDK1.8及以上 |
MySQL | 5.7+ | 5.7及以上 |
Redis | 3.2+ |  |

### 技术选型 
技术 | 版本 |  备注
-|-|-
spring boot | 2.7.18 |
Mybatis | 3.5.13 | DAO Framework |
mybatis-plus | 3.5.4.1 | mybatis增强框架 |
fastjson2 | 2.0.42 | JSON处理工具集 |
Swagger | V3 | Swagger文档 |
knife4j | 4.3.0 | api文档生成工具 |
commons-lang3 | 3.14.0 | 常用工具包 |
commons-io | 2.15.0 | IO工具包 |
commons-codec | 1.16.0 | 加密解密等工具包 |
commons-collections4 | 4.4.4 | 集合工具包 |
hibernate-validator | 6.2.5.Final | 后台参数校验注解 |
hutool-all | 5.8.23 | 常用工具集 |
lombok | 1.18.30 | 注解生成Java Bean等工具 |

### [CHANGELOG](https://gitee.com/geekidea/spring-boot-plus/blob/master/CHANGELOG.md)


### 4. 访问项目Swagger文档
[http://localhost:8888/swagger-ui/index.html](http://localhost:8888/swagger-ui/index.html)
![swagger-ui.png](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/swagger-v3.png)

### 5. 访问Knife4j文档
[http://localhost:8888/doc.html](http://localhost:8888/doc.html)
![knife4j.png](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/knife4j-doc.png)


## spring-boot-plus-vue3 前端项目
### [https://gitee.com/geekidea/spring-boot-plus-vue3](https://gitee.com/geekidea/spring-boot-plus-vue3)
#### 系统用户列表
![系统用户列表](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/user-manager.png)
#### 系统角色列表
![系统角色列表](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/role-manager.png)
#### 系统菜单列表
![系统菜单列表](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/menu-manager.png)
#### 系统部门列表
![系统部门列表](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/dept-manager-dark.png)
#### 系统日志
![系统日志列表](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/log-manager.png)
![系统日志详情](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/log-detail.png)
#### 个人中心
![个人中心](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/profile.png)

