## What is spring-boot-plus?

### A **easy-to-use**, **high-speed**, **high-efficient**, **feature-rich**, **open source** spring boot scaffolding.
> spring-boot-plus is a background rapid development framework that integrates spring boot common development components.

## Purpose
> Everyone can develop projects independently, quickly and efficiently！

## Open source License MIT-License
> Any individual or company can conduct secondary development based on this framework for commercial use without authorization!

## Features
- Integrated spring boot common development component set, common configuration, AOP log, etc
- Maven Project
- Integrated mybatis-plus fast dao operation
- Integrated Swagger/Knife4j, automatic generation of api documents
- Integrated Redis Cache
- Integration HikariCP connection pool, A solid, high-performance, JDBC connection pool at last.

## Source code directory structure
```text
spring-boot-plus
├── docs
│ ├── bin
│ │ └── install
│ ├── config
│ ├── db
│ └── img
├── logs
└── src
    ├── main
    │ ├── java
    │ │ └── io
    │ │     └── geekidea
    │ │         └── boot
    │ │             ├── auth
    │ │             ├── common
    │ │             ├── config
    │ │             ├── demo
    │ │             ├── framework
    │ │             ├── system
    │ │             ├── user
    │ │             └── util
    │ │             └── SpringBootPlusApplication.java
    │ └── resources
    │     ├── mapper
    │     └── static
    │     ├── application-dev.yml
    │     ├── application-prod.yml
    │     ├── application-test.yml
    │     ├── application.yml
    │     ├── banner.txt
    │     ├── ip2region.xdb
    │     ├── logback-spring.xml
    └── test
        ├── java
        │ └── io
        │     └── geekidea
        │         └── boot
        │             ├── generator
        │             └── system
        └── resources
            └── templates
```

### Project Environment 
Name | Version |  Remark
-|-|-
JDK | 1.8+ | JDK1.8 and above |
MySQL | 5.7+ | 5.7 and above |
Redis | 3.2+ |  |

### Technology stack 
Component| Version |  Remark
-|-|-
Spring Boot | 2.7.18 |
Mybatis | 3.5.13 | DAO Framework |
Mybatis Plus | 3.5.4.1 | mybatis Enhanced framework |
Fastjson | 2.0.42 | JSON processing toolset |
Swagger | V3 | Api document generation tool |
Knife4j | 4.3.0 | Api document generation tool |
commons-lang3 | 3.14.0 | Apache language toolkit |
commons-io | 2.15.0 | Apache IO Toolkit |
commons-codec | 1.16.0 | Apache Toolkit such as encryption and decryption |
commons-collections4 | 4.4.4 | Apache collections toolkit |
hibernate-validator | 6.2.5.Final | Validator toolkit |
hutool-all | 5.8.23 | Common toolset |
lombok | 1.18.30 | Automatically plugs |

### Project Link Diagram
![项目调用链路图](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/system-link.jpg)

### [CHANGELOG](https://github.com/geekidea/spring-boot-plus/blob/master/CHANGELOG.md)

#### Generated code structure

```text
├── controller
│ └── FooBarController.java
├── dto
│ ├── FooBarDto.java
│ └── FooBarUpdateDto.java
├── entity
│ └── FooBar.java
├── mapper
│ └── FooBarMapper.java
├── query
│ └── FooBarQuery.java
├── service
│ ├── FooBarService.java
│ └── impl
│     └── FooBarServiceImp.java
└── vo
    ├── FooBarVo.java
    └── FooBarVo.java

resources
└── mapper
    └── foobar
        └── FooBarMapper.xml    
```

```text
src/test/resources
└── templates
    ├── addDto.java.vm          Add DTO generator template
    ├── controller.java.vm      Controller generator template
    ├── entity.java.vm          Entity generator template
    ├── infoVo.java.vm          Detail VO generator template
    ├── mapper.java.vm          Mapper  generator template
    ├── mapper.xml.vm           Mapper xml  generator template
    ├── query.java.vm           Page Query  generator template
    ├── service.java.vm         Service  generator template
    ├── serviceImpl.java.vm     Service implement  generator template
    ├── updateDto.java.vm       Update DTO generator template
    └── vo.java.vm              List VO generator template
```


### 3. Startup Project
> Project Main Class: SpringBootPlusApplication  [http://localhost:8888](http://localhost:8888)

### 4. Access Swagger Docs
[http://localhost:8888/swagger-ui/index.html](http://localhost:8888/swagger-ui/index.html)
![swagger-ui.png](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/swagger-v3.png)

### 5. Access Knife4j Docs 
[http://localhost:8888/doc.html](http://localhost:8888/doc.html)
![knife4j.png](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/knife4j-doc.png)


## spring-boot-plus-vue Vue3 Project
### [GITEE-REPO](https://gitee.com/geekidea/spring-boot-plus-vue3)
#### System User List
![System User List](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/user-manager.png)
#### System Role List
![System Role List](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/role-manager.png)
#### System Menu List
![System Menu List](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/menu-manager.png)
#### System Department
![System Department List](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/dept-manager-dark.png)
#### System Log List
![System Log List](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/log-manager.png)
![System Log Detail](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/log-detail.png)
#### User Profile
![User Profile](https://geekidea.oss-cn-chengdu.aliyuncs.com/spring-boot-plus/img/profile.png)
