import {http} from '@/utils/http'

// 添加安全管控措施方案
export function addCntrolMeasure(data:any) {
    return http.post<any>('/admin/cntrolMeasure/addCntrolMeasure', data)
}

// 修改安全管控措施方案
export function updateCntrolMeasure(data:any) {
    return http.post<any>('/admin/cntrolMeasure/updateCntrolMeasure', data)
}

// 删除安全管控措施方案
export function deleteCntrolMeasure(id:string) {
    return http.post<any>('/admin/cntrolMeasure/deleteCntrolMeasure/'+id)
}

// 获取安全管控措施方案详情
export function getCntrolMeasure(id:any) {
    return http.post<any>('/admin/cntrolMeasure/getCntrolMeasure/'+id)
}

// 获取安全管控措施方案分页列表
export function getCntrolMeasurePage(data:any) {
    return http.post<any>('/admin/cntrolMeasure/getCntrolMeasurePage', data)
}





















