import {http} from '@/utils/http'

// 添加设备部件存在安全隐患事件关联关系
export function addHazardUnitEvent(data:any) {
    return http.post<any>('/admin/hazardUnitEvent/addHazardUnitEvent', data)
}

// 修改设备部件存在安全隐患事件关联关系
export function updateHazardUnitEvent(data:any) {
    return http.post<any>('/admin/hazardUnitEvent/updateHazardUnitEvent', data)
}

// 删除设备部件存在安全隐患事件关联关系
export function deleteHazardUnitEvent(id:string) {
    return http.post<any>('/admin/hazardUnitEvent/deleteHazardUnitEvent/'+id)
}

// 获取设备部件存在安全隐患事件关联关系详情
export function getHazardUnitEvent(id:any) {
    return http.post<any>('/admin/hazardUnitEvent/getHazardUnitEvent/'+id)
}

// 获取设备部件存在安全隐患事件关联关系分页列表
export function getHazardUnitEventPage(data:any) {
    return http.post<any>('/admin/hazardUnitEvent/getHazardUnitEventPage', data)
}





















