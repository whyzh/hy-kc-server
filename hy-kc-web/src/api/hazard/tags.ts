import {http} from '@/utils/http'

// 添加标签列
export function addTags(data:any) {
    return http.post<any>('/admin/tags/addTags', data)
}

// 修改标签列
export function updateTags(data:any) {
    return http.post<any>('/admin/tags/updateTags', data)
}

// 删除标签列
export function deleteTags(id:string) {
    return http.post<any>('/admin/tags/deleteTags/'+id)
}

// 获取标签列详情
export function getTags(id:any) {
    return http.post<any>('/admin/tags/getTags/'+id)
}

// 获取标签列分页列表
export function getTagsPage(data:any) {
    return http.post<any>('/admin/tags/getTagsPage', data)
}
//获取所有标签
export function getTagsList(data:any) {
    return http.post<any>('/admin/tags/getTagsList', data)
}





















