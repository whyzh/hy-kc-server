import {http} from '@/utils/http'

// 添加设备部件存在安全隐患事件列
export function addSecureEvent(data:any) {
    return http.post<any>('/admin/secureEvent/addSecureEvent', data)
}

// 修改设备部件存在安全隐患事件列
export function updateSecureEvent(data:any) {
    return http.post<any>('/admin/secureEvent/updateSecureEvent', data)
}

// 删除设备部件存在安全隐患事件列
export function deleteSecureEvent(id:string) {
    return http.post<any>('/admin/secureEvent/deleteSecureEvent/'+id)
}

// 获取设备部件存在安全隐患事件列详情
export function getSecureEvent(id:any) {
    return http.post<any>('/admin/secureEvent/getSecureEvent/'+id)
}

// 获取设备部件存在安全隐患事件列分页列表
export function getSecureEventPage(data:any) {
    return http.post<any>('/admin/secureEvent/getSecureEventPage', data)
}





















