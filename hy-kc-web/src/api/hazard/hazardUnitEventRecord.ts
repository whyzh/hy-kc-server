import {http} from '@/utils/http'

// 添加安全隐患事件处理记录
export function addHazardUnitEventRecord(data:any) {
    return http.post<any>('/admin/hazardUnitEventRecord/addHazardUnitEventRecord', data)
}

// 修改安全隐患事件处理记录
export function updateHazardUnitEventRecord(data:any) {
    return http.post<any>('/admin/hazardUnitEventRecord/updateHazardUnitEventRecord', data)
}

// 删除安全隐患事件处理记录
export function deleteHazardUnitEventRecord(id:string) {
    return http.post<any>('/admin/hazardUnitEventRecord/deleteHazardUnitEventRecord/'+id)
}

// 获取安全隐患事件处理记录详情
export function getHazardUnitEventRecord(id:any) {
    return http.post<any>('/admin/hazardUnitEventRecord/getHazardUnitEventRecord/'+id)
}

// 获取安全隐患事件处理记录分页列表
export function getHazardUnitEventRecordPage(data:any) {
    return http.post<any>('/admin/hazardUnitEventRecord/getHazardUnitEventRecordPage', data)
}





















