import {http} from '@/utils/http'

// 添加设备列
export function addHazard(data:any) {
    return http.post<any>('/admin/hazard/addHazard', data)
}

// 修改设备列
export function updateHazard(data:any) {
    return http.post<any>('/admin/hazard/updateHazard', data)
}

// 删除设备列
export function deleteHazard(id:string) {
    return http.post<any>('/admin/hazard/deleteHazard/'+id)
}

// 获取设备列详情
export function getHazard(id:any) {
    return http.post<any>('/admin/hazard/getHazard/'+id)
}

// 获取设备列分页列表
export function getHazardPage(data:any) {
    return http.post<any>('/admin/hazard/getHazardPage', data)
}
// 获取设备总列表没有分页
export function getHazardList(data:any) {
    return http.post<any>('/admin/hazard/getHazardList', data)
}





















