import {http} from '@/utils/http'

// 添加设备安全隐患部件列
export function addHazardUnit(data:any) {
    return http.post<any>('/admin/hazardUnit/addHazardUnit', data)
}

// 修改设备安全隐患部件列
export function updateHazardUnit(data:any) {
    return http.post<any>('/admin/hazardUnit/updateHazardUnit', data)
}

// 删除设备安全隐患部件列
export function deleteHazardUnit(id:string) {
    return http.post<any>('/admin/hazardUnit/deleteHazardUnit/'+id)
}

// 获取设备安全隐患部件列详情
export function getHazardUnit(id:any) {
    return http.post<any>('/admin/hazardUnit/getHazardUnit/'+id)
}

// 获取设备安全隐患部件列分页列表
export function getHazardUnitPage(data:any) {
    return http.post<any>('/admin/hazardUnit/getHazardUnitPage', data)
}





















